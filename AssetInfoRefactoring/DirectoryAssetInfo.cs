using System;
using System.Collections.Generic;
using System.IO;



public class DirectoryAssetInfo : FileSystemInfo
{
    #region fields
    #endregion

    public DirectoryAssetInfo(string fullName, DateTime lastWriteTime, List<FileSystemInfo> entries)
    {
        _fullName = fullName;
        _lastWriteTime = lastWriteTime;
        _entries = entries;
    }

    public DirectoryAssetInfo(string fullName, DateTime lastWriteTime)
    {
        _fullName = fullName;
        _lastWriteTime = lastWriteTime;
    }

    public override void Delete()
    {
        throw new NotImplementedException();
    }
    
    public override bool Exists { get; }
    public override string FullName => _fullName;
    public new FileAttributes Attributes { get; set; } = FileAttributes.Directory;
    public new DateTime LastWriteTime => _lastWriteTime;
    public override string Name { get => throw new NotImplementedException(); }

    private readonly string _fullName;
    private readonly DateTime _lastWriteTime;
	
    private readonly List<FileSystemInfo> _entries = new List<FileSystemInfo>();

    public FileSystemInfo[] GetFileSystemInfos(string searchPattern, SearchOption searchOption)
    {
        List<FileSystemInfo> ret = new List<FileSystemInfo>();
        if (searchPattern != "*" || searchOption != SearchOption.AllDirectories) throw new System.NotImplementedException();
        foreach (FileSystemInfo entry in _entries)
        {
            ret.Add(entry);
            if ((entry.Attributes & FileAttributes.Directory) == FileAttributes.Directory )
                ret.AddRange(((DirectoryAssetInfo) entry).GetFileSystemInfos("*", SearchOption.AllDirectories));
        }

        return ret.ToArray();
    }
}

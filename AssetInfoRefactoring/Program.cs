﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace AssetInfoRefactoring
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dt = new DateTime();
			// My idea is that remote directory trees can be represented like this. Because we are targeting UWP and System.IO is not available, we should do the following replacements: DirectoryInfo => IStorageFolder, FileInfo => IStorageFile, FileSystemInfo => IStorageItem
            DirectoryAssetInfo dai0 = new DirectoryAssetInfo("foo", dt, new List<FileSystemInfo>{
                new DirectoryAssetInfo(@"foo\bar", dt),
                new DirectoryAssetInfo(@"foo\baz", dt),
                new DirectoryAssetInfo(@"foo\quux", dt)
            });
            var localMenuAssetsData = new DirectoryInfo(@"d:\test\foo");
        }

        class RelativePathComparer : IEqualityComparer<FileSystemInfo>
        {
            public string localRoot;
            public string remoteRoot;
            
			// This could actually have been generated automatically by Rider :)
            public bool Equals(FileSystemInfo x, FileSystemInfo y)
            {
                if (object.ReferenceEquals(x, y)) return true;

                if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null)) return false;
                
                // compare relative paths
                return new Uri(localRoot).MakeRelativeUri(new Uri(x.FullName)) == new Uri(remoteRoot).MakeRelativeUri(new Uri(y.FullName));
            }

            public int GetHashCode(FileSystemInfo fsi)
            {
                if (object.ReferenceEquals(fsi, null)) return 0;
                return fsi.FullName == null ? 0 : fsi.FullName.GetHashCode();
            }
        }
        
        public static void DeleteOldLocalFiles(DirectoryInfo localMenuAssetsData, DirectoryAssetInfo remoteMenuAssetsData)
        {
            var localEntries = new HashSet<FileSystemInfo>(localMenuAssetsData.GetFileSystemInfos("*", SearchOption.AllDirectories));
            var remoteEntries = new HashSet<FileSystemInfo>(remoteMenuAssetsData.GetFileSystemInfos("*", SearchOption.AllDirectories));
            // Sort so that directory contents can be deleted before the directories
            var entriesWithoutRemoteTwin = localEntries.Except(remoteEntries, new RelativePathComparer {localRoot = @"d:\test", remoteRoot = ""}).OrderByDescending(e => e.FullName);
            
            foreach (FileSystemInfo fsi in entriesWithoutRemoteTwin)
            {
                fsi.Delete();
            }
        }        
    }
}
using System;
using System.IO;


public class FileAssetInfo : FileSystemInfo
{
    public FileAssetInfo(string fullName, DateTime lastWriteTime)
    {
        _fullName = fullName;
        _lastWriteTime = lastWriteTime;
    }

    public override void Delete()
    {
        throw new NotImplementedException();
    }

    public override string FullName => _fullName;
    public new DateTime LastWriteTime => _lastWriteTime;

    public override bool Exists { get; }
    public override string Name { get => throw new NotImplementedException(); }

    private readonly string _fullName;
    private readonly DateTime _lastWriteTime;
}
